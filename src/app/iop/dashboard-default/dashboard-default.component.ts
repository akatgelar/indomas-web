import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {ChartEvent, ChartType} from 'ng-chartist';
declare const $: any;
declare var Morris: any;

export interface Chart {
  type: ChartType;
  data: Chartist.IChartistData;
  options?: any;
  responsiveOptions?: any;
  events?: ChartEvent;
}

@Component({
  selector: 'app-dashboard-default',
  templateUrl: './dashboard-default.component.html',
  styleUrls: ['./dashboard-default.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DashboardDefaultComponent implements OnInit {

 content = ` Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum praesentium officia, fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur aspernatur at, eaque hic repellendus sit dicta consequatur quae, ut harum ipsam molestias maxime non nisi reiciendis eligendi! Doloremque quia pariatur harum ea amet quibusdam quisquam, quae, temporibus dolores porro doloribus.Illum praesentium officia, fugit recusandae ipsa, quia velit nulla adipisci? Consequuntur aspernatur at,`;
  timeline = [
    { caption: '16 Jan', date: new Date('2014, 1, 16'), selected: true, title: 'Kondisi Kendaraan', content: this.content },
    { caption: '28 Feb', date: new Date('2014, 2, 28'), title: 'Kondisi Kendaraan', content: this.content },
    { caption: '20 Mar', date: new Date('2014, 3, 20'), title: 'Kondisi Kendaraan', content: this.content },
    { caption: '20 May', date: new Date('2014, 5, 20'), title: 'Kondisi Kendaraan', content: this.content },
    { caption: '09 Jul', date: new Date('2014, 7, 9'), title: 'Kondisi Kendaraan', content: this.content },
    { caption: '30 Aug', date: new Date('2014, 8, 30'), title: 'Kondisi Kendaraan', content: this.content },
    { caption: '15 Sep', date: new Date('2014, 9, 15'), title: 'Kondisi Kendaraan', content: this.content },
    { caption: '01 Nov', date: new Date('2014, 11, 1'), title: 'Kondisi Kendaraan', content: this.content },
    { caption: '10 Dec', date: new Date('2014, 12, 10'), title: 'Kondisi Kendaraan', content: this.content },
    { caption: '29 Jan', date: new Date('2015, 1, 19'), title: 'Kondisi Kendaraan', content: this.content },
    { caption: '3 Mar', date: new Date('2015,  3,  3'), title: 'Kondisi Kendaraan', content: this.content },
  ];

  options = {
        size: 300
    };

  
  constructor() { }

  ngOnInit() {
    setTimeout(() => {
      var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

      Morris.Area({
        element: 'morris-extra-area',
        data: [{
            month: '2018-01', 
            value: 5
          }, {
            month: '2018-02', 
            value: 6
          }, {
            month: '2018-03', 
            value: 9
          }, {
            month: '2018-04', 
            value: 10
          }, {
            month: '2018-05', 
            value: 8
          }, {
            month: '2018-06', 
            value: 4
          }, {
            month: '2018-07',
            value: 1
          }, {
            month: '2018-08',
            value: 2
          }, {
            month: '2018-09',
            value: 0
          }, {
            month: '2018-10',
            value: 7
          }, {
            month: '2018-11',
            value: 10 
          }, {
            month: '2018-12',
            value: 6
          }  
        ],
        //lineColors: ['rgb(26, 188, 156)'],
        xkey: 'month',
        ykeys: ['value'],
        labels: ['Pekerjaan'],
        pointSize: 0,
        lineWidth: 0,
        resize: true,
        fillOpacity: 0.8,
        behaveLikeLine: true,
        gridLineColor: '#5FBEAA',
        hideHover: 'auto',
        xLabelFormat : function (x) {
          return months[x.getMonth()];
        }
      });
       

    }); 
  }

}
