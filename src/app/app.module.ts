import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppRoutes } from './app.routing';
import { AppComponent } from './app.component';
import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AdminLayoutSupirComponent } from './layouts/supir/admin-layout.component';
import { AdminLayoutMROComponent } from './layouts/mro/admin-layout.component';
import { AdminLayoutBengkelComponent } from './layouts/bengkel/admin-layout.component';
import { AdminLayoutMODAComponent } from './layouts/moda/admin-layout.component';
import { AdminLayoutIOPComponent } from './layouts/iop/admin-layout.component';
import { AdminLayoutAFPComponent } from './layouts/afp/admin-layout.component';
import { AdminLayoutDirekturComponent } from './layouts/direktur/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
import { SharedModule } from './shared/shared.module';
import { BreadcrumbsComponent } from './layouts/admin/breadcrumbs/breadcrumbs.component';
import { TitleComponent } from './layouts/admin/title/title.component';
import {ScrollModule} from './scroll/scroll.module';
import {LocationStrategy, PathLocationStrategy} from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    AdminLayoutSupirComponent,
    AdminLayoutMROComponent,
    AdminLayoutBengkelComponent,
    AdminLayoutMODAComponent,
    AdminLayoutIOPComponent,
    AdminLayoutAFPComponent,
    AdminLayoutDirekturComponent,
    AuthLayoutComponent,
    BreadcrumbsComponent,
    TitleComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    RouterModule.forRoot(AppRoutes),
    FormsModule,
    HttpModule,
    ScrollModule
  ],
  exports: [ScrollModule],
  providers: [
      { provide: LocationStrategy, useClass: PathLocationStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
