import {Injectable} from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItems {
  state: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItems[];
}

const MENUITEMS = [
  {
    label: 'Aktivitas',
    main: [ 
      {
        main_state: 'afp',
        state: 'dashboard', 
        name: 'Dashboard',
        type: 'link',
        icon: 'fa fa-home', 
      },
      {
        main_state: 'afp',
        state: 'pekerjaan-list', 
        name: 'Pekerjaan',
        type: 'link',
        icon: 'fa fa-cube', 
      },
      {
        main_state: 'afp',
        state: 'laporan-list',
        name: 'Laporan Harian',
        type: 'link',
        icon: 'fa fa-file', 
      },
      {
        main_state: 'afp',
        state: 'order-sparepart-list',
        name: 'Pengajuan Suku Cadang',
        type: 'link',
        icon: 'fa fa-puzzle-piece', 
      }, 
      {
        main_state: 'afp',
        state: 'order-service-list',
        name: 'Pengajuan Service',
        type: 'link',
        icon: 'fa fa-wrench', 
      },
      {
        main_state: 'afp',
        state: 'order-service-list',
        name: 'Keuangan',
        type: 'link',
        icon: 'fa fa-money', 
      },
    ]
  },
  {
    label: 'Master',
    main: [ 
      {
        main_state: 'afp',
        state: 'master-kendaraan-list',
        name: 'Master Truk',
        type: 'link',
        icon: 'fa fa-truck', 
      },
      {
        main_state: 'afp',
        state: 'master-afp-list',
        name: 'Master afp',
        type: 'link',
        icon: 'fa fa-child', 
      },
      {
        main_state: 'afp',
        state: 'master-user-list',
        name: 'Master User',
        type: 'link',
        icon: 'fa fa-users', 
      },
      {
        main_state: 'afp',
        state: 'master-user-list',
        name: 'Master User Level',
        type: 'link',
        icon: 'fa fa-signal', 
      }
    ],
  }, 
  {
    label: 'Perlengkapan',
    main: [  
      {
        main_state: 'afp',
        state: 'master-sparepart-list',
        name: 'Master Jenis Suku Cadang',
        type: 'link',
        icon: 'fa fa-th-large', 
      },
      {
        main_state: 'afp',
        state: 'master-sparepart-list',
        name: 'Master Suku Cadang',
        type: 'link',
        icon: 'fa fa-th-list', 
      },
      {
        main_state: 'afp',
        state: 'master-sparepart-list',
        name: 'Master Supplier',
        type: 'link',
        icon: 'fa fa-th', 
      },
      {
        main_state: 'afp',
        state: 'master-sparepart-list',
        name: 'Master Satuan',
        type: 'link',
        icon: 'fa fa-list', 
      },
      {
        main_state: 'afp',
        state: 'master-sparepart-list',
        name: 'Master Tools',
        type: 'link',
        icon: 'fa fa-list-ul', 
      }, 
    ]
  }
];

@Injectable()
export class MenuItemsAFP {
  getAll(): Menu[] {
    return MENUITEMS;
  }

  /*add(menu: Menu) {
    MENUITEMS.push(menu);
  }*/
}
