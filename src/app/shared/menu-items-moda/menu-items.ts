import {Injectable} from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItems {
  state: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItems[];
}

const MENUITEMS = [
  {
    label: 'Aktivitas',
    main: [ 
      {
        main_state: 'moda',
        state: 'dashboard', 
        name: 'Dashboard',
        type: 'link',
        icon: 'fa fa-home', 
      },
      {
        main_state: 'moda',
        state: 'pekerjaan-list', 
        name: 'Pekerjaan',
        type: 'link',
        icon: 'fa fa-cube', 
      },
      {
        main_state: 'moda',
        state: 'laporan-list',
        name: 'Laporan Harian',
        type: 'link',
        icon: 'fa fa-file', 
      },
      {
        main_state: 'moda',
        state: 'order-sparepart-list',
        name: 'Pengajuan Suku Cadang',
        type: 'link',
        icon: 'fa fa-puzzle-piece', 
      }, 
      {
        main_state: 'moda',
        state: 'order-service-list',
        name: 'Pengajuan Service',
        type: 'link',
        icon: 'fa fa-wrench', 
      },
      {
        main_state: 'moda',
        state: 'order-service-list',
        name: 'Keuangan',
        type: 'link',
        icon: 'fa fa-money', 
      },
    ]
  },
  {
    label: 'Master',
    main: [ 
      {
        main_state: 'moda',
        state: 'master-kendaraan-list',
        name: 'Master Truk',
        type: 'link',
        icon: 'fa fa-truck', 
      },
      {
        main_state: 'moda',
        state: 'master-moda-list',
        name: 'Master moda',
        type: 'link',
        icon: 'fa fa-child', 
      },
      {
        main_state: 'moda',
        state: 'master-user-list',
        name: 'Master User',
        type: 'link',
        icon: 'fa fa-users', 
      },
      {
        main_state: 'moda',
        state: 'master-user-list',
        name: 'Master User Level',
        type: 'link',
        icon: 'fa fa-signal', 
      }
    ],
  }, 
  {
    label: 'Perlengkapan',
    main: [  
      {
        main_state: 'moda',
        state: 'master-sparepart-list',
        name: 'Master Jenis Suku Cadang',
        type: 'link',
        icon: 'fa fa-th-large', 
      },
      {
        main_state: 'moda',
        state: 'master-sparepart-list',
        name: 'Master Suku Cadang',
        type: 'link',
        icon: 'fa fa-th-list', 
      },
      {
        main_state: 'moda',
        state: 'master-sparepart-list',
        name: 'Master Supplier',
        type: 'link',
        icon: 'fa fa-th', 
      },
      {
        main_state: 'moda',
        state: 'master-sparepart-list',
        name: 'Master Satuan',
        type: 'link',
        icon: 'fa fa-list', 
      },
      {
        main_state: 'moda',
        state: 'master-sparepart-list',
        name: 'Master Tools',
        type: 'link',
        icon: 'fa fa-list-ul', 
      }, 
    ]
  }
];

@Injectable()
export class MenuItemsMODA {
  getAll(): Menu[] {
    return MENUITEMS;
  }

  /*add(menu: Menu) {
    MENUITEMS.push(menu);
  }*/
}
