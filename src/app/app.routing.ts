import {Routes} from '@angular/router';

import {AdminLayoutComponent} from './layouts/admin/admin-layout.component';
import {AdminLayoutSupirComponent} from './layouts/supir/admin-layout.component';
import {AdminLayoutMROComponent} from './layouts/mro/admin-layout.component';
import {AdminLayoutBengkelComponent} from './layouts/bengkel/admin-layout.component';
import {AdminLayoutMODAComponent} from './layouts/moda/admin-layout.component';
import {AdminLayoutIOPComponent} from './layouts/iop/admin-layout.component';
import {AdminLayoutAFPComponent} from './layouts/afp/admin-layout.component';
import {AdminLayoutDirekturComponent} from './layouts/direktur/admin-layout.component';
import {AuthLayoutComponent} from './layouts/auth/auth-layout.component';

export const AppRoutes: Routes = [
  {
    path: '',
    redirectTo: 'supir/dashboard',
    pathMatch: 'full'
  },
  {
    path: 'supir',
    component: AdminLayoutSupirComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: './supir/dashboard-default/dashboard-default.module#DashboardDefaultModule',
      },
    ]
  },
  {
    path: 'mro',
    component: AdminLayoutMROComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: './mro/dashboard-default/dashboard-default.module#DashboardDefaultModule',
      },
    ]
  },
  {
    path: 'bengkel',
    component: AdminLayoutBengkelComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: './bengkel/dashboard-default/dashboard-default.module#DashboardDefaultModule',
      },
    ]
  },
  {
    path: 'moda',
    component: AdminLayoutMODAComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: './moda/dashboard-default/dashboard-default.module#DashboardDefaultModule',
      },
    ]
  },
  {
    path: 'iop',
    component: AdminLayoutIOPComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: './iop/dashboard-default/dashboard-default.module#DashboardDefaultModule',
      },
    ]
  },
  {
    path: 'afp',
    component: AdminLayoutAFPComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: './afp/dashboard-default/dashboard-default.module#DashboardDefaultModule',
      },
    ]
  },
  {
    path: 'direktur',
    component: AdminLayoutDirekturComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: './direktur/dashboard-default/dashboard-default.module#DashboardDefaultModule',
      },
    ]
  },




  {
    path: 'auth',
    component: AuthLayoutComponent,
    children: [
      {
        path: 'login',
        loadChildren: './auth/login/login.module#LoginModule',
      },
      {
        path: 'forgot',
        loadChildren: './auth/forgot/forgot.module#ForgotModule',
      },
    ]
  },  
  {
    path: 'error',
    component: AuthLayoutComponent,
    loadChildren: './error/error.module#ErrorModule'
  }, 
  {
    path: 'offline',
    component: AuthLayoutComponent,
    loadChildren: './maintenance/offline-ui/offline-ui.module#OfflineUiModule'  
  },


 
  {
    path: '**',
    redirectTo: 'error/404'
  }
];
 